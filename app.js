// loads environment variables
require('dotenv/config');
const express = require('express');
const cors = require('cors');
const Github = require('./src/Github');
const utils = require('./src/Utils');
const DB = require('./src/DbUtils');


const app = express();
const port = process.env.PORT || 3000;

const client = new Github({
  token: process.env.OAUTH_TOKEN,
});


// Enable CORS for the client app
app.use(cors());

function getUserStats(username) {
  return client.userLanguages(username)
    .then(utils.getReposLanguagesStats);
}

app.get('/languages/repo/:username/:reponame', (req, res, next) => { // eslint-disable-line no-unused-vars
  const fullRepoName = `${req.params.username}/${req.params.reponame}`;
  client.repoLanguages(fullRepoName)
    .then(result => {
      DB.newComparison(fullRepoName).then(
        comparison => res.send({ id: comparison.id, stats: result }),
      );
    })
    .catch(next);
});

app.get('/languages/user/:comparisonId/:username', (req, res, next) => { // eslint-disable-line no-unused-vars
  DB.addUser(req.params.comparisonId, req.params.username)
    .then(user => {
      return getUserStats(req.params.username)
        .then(stats => res.send({ id: user.id, stats }));
    })
    .catch(next);
});

app.get('/user/:username', (req, res, next) => {
  client.user(req.params.username).then(
    user => res.send(user),
  )
    .catch(next);
});

app.delete('/comparison/:comparisonId/user/:userId', (req, res, next) => {
  DB.removeUser(req.params.comparisonId, req.params.userId)
    .then(() => {
      res.status(204);
      res.send();
    })
    .catch(next);
});

app.get('/latests', (req, res, next) => {
  DB.latests().then(results => {
    res.send(results);
  }).catch(next);
});

app.get('/comparison/:comparisonId', (req, res, next) => {
  DB.getComparison(req.params.comparisonId)
    .then(comparison => {
      client.repoLanguages(comparison.repoName)
        .then(result => {
          const userStats = user => getUserStats(user.name)
            .then(stats => { return { id: user.id, name: user.name, stats }; });
          Promise.all(comparison.users.map(userStats)).then(
            results => {
              const content = {
                id: comparison.id,
                stats: result,
                users: results,
              };
              res.send(content);
            },
          );
        });
    })
    .catch(next);
});

// Forward 404 to error handler
app.use((req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
});

// Error handler
app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  console.error(err);
  res.status(err.status || 500);
  res.send(err.message);
});

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server listening at http://localhost:${port}`);
});
