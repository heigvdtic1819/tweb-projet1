const BASE_URL = 'https://api.github.com';
const fetch = require('node-fetch');
const ResponseError = require('./ResponseError');

class Github {
  constructor({
    token,
    baseUrl = BASE_URL,
  }) {
    this.token = token;
    this.baseUrl = baseUrl;
  }

  setToken(token) {
    this.token = token;
  }

  request(path, opts = {}) {
    const url = `${this.baseUrl}/${path}`;
    const options = {
      ...opts,
      headers: {
        Accept: 'application/vnd.github.v3+json',
        Authorization: `token ${this.token}`,
      },
    };
    return fetch(url, options)
      .then(res => res.json()
        .then(data => {
          if (!res.ok) {
            throw new ResponseError(res, data);
          }
          return data;
        }));
  }


  user(username) {
    return this.request(`users/${username}`);
  }

  repos(username) {
    return this.request(`users/${username}/repos`);
  }

  repoLanguages(repoName) {
    return this.request(`repos/${repoName}/languages`);
  }

  userLanguages(username) {
    return this.repos(username)
      .then((repos) => {
        const getLanguage = repo => this.repoLanguages(repo.full_name);
        return Promise.all(repos.map(getLanguage));
      });
  }
}


module.exports = Github;
