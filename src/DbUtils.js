const mongoose = require('mongoose');
const ResponseError = require('./ResponseError');

const dbUrl = 'mongodb://githubheig:githubheig$2018@cluster0-shard-00-00-inrvv.mongodb.net:27017,cluster0-shard-00-01-inrvv.mongodb.net:27017,cluster0-shard-00-02-inrvv.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true';


mongoose.connect(dbUrl, { useNewUrlParser: true });

const userSchema = new mongoose.Schema({
  name: String,
});

const comparisonSchema = new mongoose.Schema({
  repoName: String,
  createdAt: Date,
  users: [userSchema],
});

const Comparison = mongoose.model('Comparison', comparisonSchema);
const User = mongoose.model('User', userSchema);


class DBUtils {
  static newComparison(repoName) {
    const comparison = new Comparison({ repoName, createdAt: new Date() });
    return comparison.save();
  }

  static getComparison(id) {
    return Comparison.findById(id).catch(() => {
      throw new ResponseError({ status: 404, url: 'comparison schema' }, { message: 'Comparison not found.' });
    });
  }

  static addUser(repoId, userName) {
    return this.getComparison(repoId).then(
      comparison => {
        const user = new User({ name: userName });
        comparison.users.push(user);
        comparison.save();
        return user;
      },
    );
  }

  static removeUser(repoId, userId) {
    return this.getComparison(repoId).then(
      comparison => {
        comparison.users.id(userId).remove();
        return comparison.save();
      },
    );
  }

  static latests() {
    return Comparison.find().sort({ createdAt: -1 }).limit(10);
  }
}


module.exports = DBUtils;
