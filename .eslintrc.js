module.exports = {
    "extends": "eslint-config-airbnb-base",
    "rules": {
        "arrow-body-style": "off",
        "arrow-parens": "off",
        "no-console": ["error", { "allow": ["warn", "error"] }],
    },
};