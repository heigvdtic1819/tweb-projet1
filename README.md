#### Aebischer Lenny - Mosca Alexandre - 30.09.2018
# TWEB - Github analysis
## Introduction

Ce document présente le projet 1 du cours de TWEB, il portera principalement sur l'utilisation de l'API Rest de Github afin de traiter certaines données reçues et les mettre en avant dans une application web.

## Description du projet

Notre projet permet de sélectionner un repository github, puis d'aller y confronter des utilisateurs pour analyser s'ils sont compatibles avec les langages utilisés. Par exemple si nous avons un repository pour une application développées en Angular, nous aurons probablement les langages HTML, CSS, JS, Typescript.
A partir de là, nous allons pouvoir comparer des utilisateurs à ce repository et sortir un classement des plus compatibles avec un pourcentage de compatibilité en fonction des langages pratiqués par ceux-ci.
Il sera également possible de partager les résultats de la comparaison à quelqu'un d'autre en utilisant le lien unique pas comparaison. Ce lien permettra de reprendre les données inscrites (repository et users) pour les utiliser à un autre momnet (reprise des statistiques, partage, etc)

## Technologies

En ce qui concerne les technologies qui sont utilisées dans ce projet, nous avons décidé d'utiliser:

- Client: un framework que nous métrisons à savoir **AngularJS**, nous n'utiliserons pas de template afin de nous laisser le choix complet de l'interface. Concernant l'affichage des informations nous utiliserons la bibliothèque Chart.js

- Serveur:  **NodeJS**, avec quoi nous avons quelques notions.

- DB: **MongoDB**, grande première

## Architecture

Concernant l'architecture de notre application, rien de bien compliqué:

![Untitled](https://i.imgur.com/lzKAzUy.png)

Le client se connectant sur notre application AngularJS depuis un navigateur conversera directement avec notre serveur en NodeJS et non pas directement avec l'API de Github, ceci afin de stocker de manière sécurisée notre jeton de connexion à Github et ainsi bénéficier de 5'000 requêtes par heure (contre 60 en anonyme).

Notre base de données contiendra tout l'historique des comparaisons effectuées permettant de les reprendre à tout moment.
Pour terminer, c'est bien notre serveur qui s'occupera de converser avec l'API de github et ainsi répondre au client qui mettra à jour les informations de son interface.

## Serveur

Le serveur utilise le framework minimaliste **ExpressJS**. Pour le lancer en local, il faut d'abord renommer le fichier `.env.default` en `.env` à la racine et remplir le paramètre OAUTH_TOKEN avec votre clé Github.

Ensuite il faut lancer la commande `npm install` pour installer les dépendances

Et finalement `npm run dev` pour lancer le serveur.

Le serveur est également accessible publiquement à l'adresse [https://tweblabo01.herokuapp.com/](https://tweblabo01.herokuapp.com/)

La base de données est stockée dans le cloud. Si vous souhaitez modifier ce comportement, vous pouvez modifier l'URL dans le fichier `DbUtils.js`.

Les tests unitaires peuvent être lancés via la commande `npm test`

## Client

Par défaut, le client se connecte à l'adresse publique du serveur (voir ci-dessus). Pour changer ce comportement, vous pouvez changer l'URL par votre URL local dans le fichier `src/services/api.service.ts`.

Comme pour le serveur, il faut d'abord faire `npm install` pour installer les dépendances. 

Pour lancer en local le projet, il suffit d'utiliser la commande `ng serve`

**Attention**, `ng` est requis pour lancer cette commande. (`npm install -g ng`)

Pour générer les fichiers permettant de déployer l'application, il est possible d'utiliser `ng build --prod`. Ce qui va créer un dossier dist contenant tous les fichiers de l'application compressés. Il suffit ensuite d'ouvrir le fichier `index.html`. Attention toutefois, sans un serveur, le routing ne fonctionnera pas correctement.

## Déploiement

Nous avons utilisé Heroku pour déployer notre application. Le client et le serveur sont sur des projets Heroku ainsi que sur des repositories git différents. Nous avons utilisé Bitbucket pipeline pour déployer automatiquement l'application sur Heroku suite à un push sur la branche master de notre projet. De plus, nous avons dû utiliser un petit serveur `expressjs` pour le client afin que la redirection se fasse correctement.


